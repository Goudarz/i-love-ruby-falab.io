== Composite Pattern

Take a look at the code link:code/design_patterns/boiling_egg.rb[boiling_egg.rb] below. Type and execute it

[source, ruby, linenums]
----
include::code/design_patterns/boiling_egg.rb[]
----

Output

----
Time (in minutes) to make boiled eggs is 30.
----

In Composite Pattern, we divide a Class into many Classes. Say if boiling eggs need to represented in a Object Oriented way, we may write `BoiledEggs` as a class as shown below.

[source, ruby]
----
class BoiledEggs < Node
  def initialize
    super "Boiled eggs"
    add_child BuyEggs.new
    add_child BoilEggs.new
    add_child PeelEggs.new
    add_child ServeEggs.new
  end

  def total_time_in_minutes
    total = 0

    for child in self.children
      total += child.time_in_minutes
    end

    total
  end
end
----

`BoiledEggs` inherits a class called `Node` which helps is to build a tree structure. Lets see about the `Node` class later. Lets now concentrate on composite pattern. As you can see in the `initialize` method, in this class `BoiledEggs` is partitioned into many class classes namely `BuyEggs` to `ServeEggs`, and they are been added as child nodes to `BoiledEggs`.

If you can see the class `BuyEggs` its like this:

[source, ruby]
----
class BuyEggs < Node
  attr_accessor :time_in_minutes

  def initialize
    @time_in_minutes = 15
    super "Buy eggs"
  end
end
----

If you see the class `ServeEggs`, its like this:

[source, ruby]
----
class ServeEggs < Node
  attr_accessor :time_in_minutes

  def initialize
    @time_in_minutes = 2
    super "Serve eggs"
  end
end
----

In fact it looks very similar to `BuyEggs`, and hence these classes can be treated in a similar way. So its like this `BoiledEggs` composes of different objects which can be treated in a similar way.

Now each class in `BoiledEggs` have a `time_in_minutes` attribute. Hence if we need to can calculate total time in minutes for making boiled eggs, all we need to do is to write a function `total_time_in_minutes` as shown

[source, ruby]
----
class BoiledEggs < Node
 ...

  def total_time_in_minutes
    total = 0

    for child in self.children
      total += child.time_in_minutes
    end

    total
  end
end
----

So Composite pattern can be employed in places where there is a complex object, who's functionality can be broken into smaller objects, these smaller objects can be treated in very similar fashion, and these smaller objects are placed in a tree structure.

Talking about tree structure, we have built a class called `Node` which can be found in file link:code/design_patterns/node.rb[node.rb]. You can see it below.

[source, ruby, linenums]
----
include::code/design_patterns/node.rb[]
----

It has a function called `add_child` with which we can add child elements. You can get parent of a node with `parent` function. To see how this node class works, take a look at the code link:code/design_patterns/composite.rb[composite.rb] below:

[source, ruby, linenums]
----
include::code/design_patterns/composite.rb[]
----

Output

----
children of <Node: n1, id: 47253445808700> are:
<Node: n2, id: 47253445808560>
<Node: n3, id: 47253445808360>

Parent of <Node: n3, id: 47253445808360> is <Node: n1, id: 47253445808700>
----
