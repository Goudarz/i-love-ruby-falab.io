== Modules and Mixins

When ever you think of modules, you think of a box or something. Just look at your printer. Its a module. It can do some thing. It has things to do something. In a similar way modules in Ruby can contain Ruby code to do something. Modules are way to pack Ruby code into possible logic units. When ever you want to use the code in a module, you just include it in your Ruby program.

Lets look at our first module program called link:code/module_function.rb[module_function.rb]. The program below has two modules namely `Star` and `Dollar`. Both these modules have the same function called `line`. Note that in the function `line` in module `Star`, we print a line of 20 star (*) characters. In similar fashion in function `line` in module `Dollar` we print a line of 20 dollar ($) characters. Type the program in your text editor and execute it.

[source, ruby]
----
include::code/module_function.rb[]
----

Output

----
********************
$$$$$$$$$$$$$$$$$$$$

----

Lets look at the program that's outside the module. We have the following code:

[source, ruby]
----
include Star
line
include Dollar
line
----

In the line `include Star`, we include the code that's in the `Star` module, then we call the function `line`, so we get output as a line of 20 stars. Look at the next line, we include the module `Dollar`. `Dollar` too has a function called `line`. Since this module is called after `Star`, the `line` function in Dollar module over writes  or in the right terms hides the `line` function in the `Star` module. Hence calling `line` after `include Dollar` will execute code in the `line` function of `Dollar` module. Hence we get a line of twenty dollar sign.

In the coming example link:code/module_function_0.rb[module_function_0.rb] we will see what happens when we call the `line` function without including any module. Type the program below and execute it

[source, ruby]
----
include::code/module_function_0.rb[]
----

Output

----
module_function_0.rb:15:in `<main>': undefined local variable or method `line' for main:Object (NameError)
----

As you can see that `line` is considered as undefined local variable or a methodfootnote:[Methods are another name for function]. So we can say that the functions in module can be accessed only if the module is included in your program.

Lets say that we write another module without any function but just code in it. I wrote the following program linclude:code/module.rb[module.rb] just because I want to see what happens. As it turns out, when module is coded in a Ruby file and executed, the code in module gets executed by default. This happens even if we don't include the module.

[source, ruby]
----
include::code/module.rb[]
----

Output

----
Something
Nothing
----

The output of the above program link:code/module.rb[module.rb] prints out `Something` and `Nothing`. We have put two modules called `Something` which contains the code `puts “Something”` and another module `Nothing` which contains `puts “Nothing”`. Though I haven't included these modules in my program using `include` statement, the code under them gets executed anyway.

=== Calling functions without include

In the program link:code/module_function.rb[module_function.rb],  we have seen how to include module and call the function(s) in it. We printed a line of stars and dollars. Lets do the same in a different way. This time we wont be using the include keyword.

Type the program link:code/module_function_1.rb[module_function_1.rb] and execute it.

[source, ruby]
----
include::code/module_function_1.rb[]
----

Output

----
$$$$$$$$$$$$$$$$$$$$
********************
$$$$$$$$$$$$$$$$$$$$
----

Take a look at the following code:

[source, ruby]
----
Dollar::line
Star::line
Dollar::line
----

When we call `Dollar::line`, the `line` function in `Dollar` module gets executed. When we call `Star::line`, the `line` function in the `Star` module gets executed. So when you want to call a function in module just use the following syntax `<module-name>::<function-name>`.

Note that in module `Star`, we have defined the function line as `Star.line` and not just `line`. Similarly in module `Dollar` we have defined it as `Dollar.line`.

OK, we are getting to know about modules, now lets get our hands really dirty. Type the code below (link:code/module_function_2.rb[module_function_2.rb]) and execute it.

[source, ruby]
----
include::code/module_function_2.rb[]
----

Output

----
$$$$$$$$$$$$$$$$$$$$
********************
$$$$$$$$$$$$$$$$$$$$
@@@@@@@@@@@@@@@@@@@@
----

OK you have got some output. Take a look at the following lines

[source, ruby]
----
include At

Dollar::line
Star::line
Dollar::line
line
----

Note that we have included the module `At` at first using the `include At` statement. While executing the `Dollar::line` statement, we get an output of twenty dollars which forms a line. While executing `Star::line` we get a output of twenty stars. Next we once again call `Dollar::line`, then comes the catch. We just call the function `line`. Since we have included `At` at first, when the statement `line` is encountered it calls the line method in `At` module gets called. This shows that though we have called `Dollar::line` and `Star::line` , it does not includefootnote:[Or mixin] the module code in the program, instead it just executes the particular function in the module.

In link:code/module_function _1.rb[module_function _1.rb], we have seen how we can call a function in a module say `Star::line`, where `Star` is the module name and `line` is the function name. To do so in `Star` module we have defined the function `line` as follows

[source, ruby]
----
def Star.line
	puts '*' * 20
end
----

Where instead of naming it just `line`, we have named it `Star.line`.  Note that link:code/module_function_3.rb[module_function_3.rb] is similar to link:code/module_function_1.rb[module_function_1.rb], but take a deep look into  `line` function in `Dollar` module. It is not named `Dollar.line`, instead its named `Star.line` . What will happen if we mess up the code like this? Execute the program below and see.

[source, ruby]
----
include::code/module_function_3.rb[]
----

Output

----
@@@@@@@@@@@@@@@@@@@@
$$$$$$$$$$$$$$$$$$$$
@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@
----

Notice that whenever we call `Dollar::line` , the `line` function in `At` module is called.  Its because since we have defined it as `Star.line` in `Dollar` module, `Dollar::line` does not exist and hence the function `line` in the `At` module is called.  Note that we have included `At` module using the statement include `At`.

We now consider another scenario where (see program link:code/module_function_4.rb[module_function_4.rb]) in the `Dollar` module we just define the function `line` as `line` and not as `Dollar.line`. When we call it using `Dollar::line` in the program below, we see that the `line` function in the `At` module gets called. So the moral of the story is, if you are calling `<module-name>::<function-name>` in your program, make sure that the function is named `<module-name>.<function-name>` inside the module.

[source, ruby]
----
include::code/module_function_4.rb[]
----

Output

----
@@@@@@@@@@@@@@@@@@@@
********************
@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@
----

=== Classes in modules

We have seen how the Ruby interpreter behaves to functions in modules. Lets now see how it behaves to classes in modules. Type the program below link:code/module_class.rb[module_class.rb] and execute it.

[source, ruby]
----
include::code/module_class.rb[]
----

Output

----
I govern this land
I take measurements
----

Lets analyze the program. We have two modules. The first one is named `Instrument`, and the second one is named `People`. Both have a class named `Ruler` and both Ruler's have method named `what_u_do?` . Fine, now lets come to the program. We have a statement `r1 = People::Ruler.new` in which `r1` becomes an instance variable of `Ruler` class in `People` module. Similarly we have `r2 = Instrument::Ruler.new` in which `r2` becomes instance variable of `Ruler` class in `Instrument` module.

This can be verified by executing the following code

[source, ruby]
----
r1.what_u_do?
r2.what_u_do?
----

In which calling `r1.what_u_do?`` outputs `I govern this land` and calling `r2.what_u_do?` outputs `I take measurements`.

The moral of the story is you can have same class names in different modules, to call that class just use `<module-name>::<class-name>`. .

=== Mixins

Another use of modules is that you can mix code in modules as you wish. This one is called mixin. We have already seen about mixins but I haven't told you that it was mixin. For example lets say that you are writing some application in Ruby for Linux and Apple machine. You find that some code works only on Linux and other works only on Apple, then you can separate them as shown  below. The Apple stuff goes into Apple module and Linux stuff goes into the Linux module.

Lets say that your friend uses a Linux machine and wants to run your program. All you need to do is to include Linux in your code as shown below.

[source, ruby]
----
include::code/mixin.rb[]
----

Output

----
This function contains code for Linux systems
----

When the method `function` is called, the method `function` in `Linux` module will be called. In short you have mixed in Linux code in your program and have kept out the Apple stuff.

Lets see another example of mixin. Take a look at the code link:code/mixin_2.rb[mixin_2.rb] below. Lets say that your client tells that he is badly need of a program that computes the area of circle and volume of sphere. So you develop two classes called `Circle` and `Sphere` and equip with code to find area and volume. So your client is happy. Since your client is in Milkyway galaxy, the constant Pifootnote:[Pi is a mathematical constant used to find area of circle and volume of sphere. Unless you are Einstien, don't bother about it.] is 22 divided by 7. So we have put the value of `Pi` in a module named `Constants` and have included in `Circle` and `Sphere` class using the statement `include Constants`. Type in the program below and execute it.

[source, ruby]
----
include::code/mixin_2.rb[]
----

Output

----
Circle Area = 154.0
Sphere Volume = 1437.333333333333
----

So you get something as output. You might ask whats so great in putting a constant in a module and mixing it in a class using `include` statement. Well the above program teaches you two morals

* You can put constants in a module
* If you have common codefootnote:[Common functions and constants] that can be shared between classes, you can put it into module and share it.

If you have defined the value of `Pi` separately in each class and if you happened to get a client from Andromeda galaxy where Pi is 57 divided by 18.1364, you can make change just in one place, that is in `Constants` module and see the change reflect in many classes (`Circle` and `Sphere` classes in our case).

Thus moral is modules help us to cater customers who are beyond our own galaxy and we can truly build a galactic empirefootnote:[Somewhat like a Star Wars thing. So if you have a dream to become Galactic Emperor, study Ruby].

