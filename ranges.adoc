== Ranges

Some times we need to have a range of values, for example in a grading system. If a student scores from 60 to 100 marks, his grade is A, from 50 to 59 his grade is B and so on. When ever we need to deal with a range of values we can use ranges in Ruby. Type `irb  --simple-prompt` in your terminal and type these into it

[source, ruby]
----
>> (1..5).each {|a| print "#{a}, " }
----

Output

----
1, 2, 3, 4, 5, => 1..5
----

OK whats that `(1..5)` in the above statement, this is called Range. Range is a object that has got an upper value and a lower value and all values in between. Note that like array, each and every value in a range can be got out using a `each` method as shown above.

Range does not work only on numbers it can work on strings too as shown below

[source, ruby]
----
>> ("bad".."bag").each {|a| print "#{a}, " }
----

Output

----
bad, bae, baf, bag, => "bad".."bag"
----

Lets try out another few examples in our irb that will tell to us more about Ranges. So fire up your irb and type the following

[source, ruby]
----
>> a = -4..10
----

Output

----
=> -4..10
----

In the above code snippet we create a range that ranges from value -4 to 10. To check what type `a` belongs lets find out what class it is

[source, ruby]
----
>> a.class
----

Output

----
=> Range
----

As we can see a belongs to `Range` class

To get the maximum value in a range use the `max` method as shown

[source, ruby]
----
>> a.max
----

Output

----
=> 10
----

To get the minimum in a range use the `min` method as shown

[source, ruby]
----
>> a.min
----

Output

----
=> -4
----

Its possible to convert range to an array by using `to_a` method as shown

[source, ruby]
----
>> a.to_a
----

Output

----
=> [-4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
----

=== Ranges used in case .. when

Look at the program below ( link:code/ranges_case_when.rb[ranges_case_when.rb] ), we are building a student grading system in which when a mark is entered the program puts out the grade of the student, study the code, type it and execute it, we will soon see how it works.

[source, ruby]
----
include::code/ranges_case_when_19.rb[]
----

Output

----
Enter student mark: 72
Your grade is B
----

At first the program prints that the software is student grading system and asks the user to enter the student mark. When the mark is entered its got using `gets` statement, the trailing newline character is chopped using the `chop` method and its converted to integer using the `to_i` method and the mark is stored in the variable mark. All of it is done using this `mark = gets.chop.to_i` statement.

Once we have the mark,  we need to compare it with a range of values to determine the grade which is done using the following statements:

[source, ruby]
----
grade = case mark
	when 80..100  : 'A'
	when 60..79   : 'B'
	when 40..59   : 'C'
	when 0..39    : 'D'
	else "Unable to determine grade. Try again."
end
----

Here we see that mark is passed to the case statement. In the `when` we don't have a number or string to compare the `mark`, in fact we have ranges. When the mark lies from 80 to 100 (both inclusive) the grade is set to `A`, when its lies in 60 to 79 its set to `B`, `C` for 40 to 59 and `D` for 0 to 39. If the user enters something wrong, grade will be set to `"Unable to determine grade. Try again."`.

So as we can see ranges come very handy when they are used with `case when` statement. It makes programming relatively simple when compared to other languages.

=== Checking Intervals

Another use of Ranges is to check if any thing is located in a particular interval. Consider the program (link:code/ranges_cap_or_small.rb[ranges_cap_or_small.rb] ) below

[source, ruby]
----
include::code/ranges_cap_or_small.rb[]
----

Output

----
Enter any letter: R
You have entered a upper case letter
----

Read it carefully and execute it. In the above case I have entered capital `R` and hence the program says I have entered a upper case letter. If I had entered a lower case letter, the program would have said I had entered a lower case letter. Lets see how the program works. The following lines:

[source, ruby]
----
print "Enter any letter: "
letter = gets.chop
----

prompts the user to enter a letter, when the user enters a letter the `gets` method gets it, `chop` chops off the new line character thats added due to the enter key we press. In the next line look at the `if ('a'..'z') === letter` , here we check if the value in variable letter lies with `'a'` and `'z'` (both inclusive) , if it does, we print that user has entered small letter. Note that we don't use double equal to `== `but we use triple equal to `===`footnote:[This triple equal to === is technically called case equality operator.] to check if its in range. In a similar way `('A'..'Z') ===` letter returns true if letter has capital letter in it and the program prints the user has entered a capital letter.

=== Using triple dots ...

Another thing in Range I would like to add is using triple dots instead of using double dots. Just try these out on your irb.

[source, ruby]
----
>> (1..5).to_a
=> [1, 2, 3, 4, 5]
>> (1...5).to_a
=> [1, 2, 3, 4]
----

See from above code snippet when I type `(1..5).to_a` we get an array output as `[1, 2, 3, 4, 5]` , but for `(1...5).to_a` we get output as `[1, 2, 3, 4]` . The triple dots ignores the last value in range.

=== Endless Ranges

Ruby has endless ranges, that is you can write programs like this

[source, ruby]
----
include::code/ranges_cap_or_small.rb[]
----

Running the program produces the following output

----
Enter your age: 32
You are grownup
----

In the above execution I have given my age as 32, and hence it comes to this part of the code

[source, ruby]
----
when (19..)
  puts "You are grownup"
----

In it look at the `(19..)`, here it means 19 and any value above it. Note that we aren't specifying `when 19..` as this would confuse the Ruby interpreter and would throw out an error.

