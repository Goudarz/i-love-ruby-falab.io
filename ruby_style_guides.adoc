== Ruby Style Guides

So you have learned about basics of Ruby programming and must have probably know how to look up Rdoc. This chapter will tell about Ruby style guides. Once every software company had a style of coding, One when inducted into a company had to follow a huge manual which defined the style of coding, that was hell.

As internet and collaboration flourished and evil Microsoft was beaten by free softwarefootnote:[http://fsf.org], commonality developed, language started to have patterns that were defined more openly and in a democratic way than controlled by few corporations and their satellite companies. So Ruby too has its own style guides.

If you follow it, and if a fellow Rubyist sees your code, you could be a respected developer. You can get all about Ruby style guides here https://github.com/bbatsov/ruby-style-guide . Hope you people follow it to be a proud Rubyist.
