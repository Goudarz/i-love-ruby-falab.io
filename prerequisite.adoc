== Prerequisite

This book provides you with enough knowledge to learn Ruby from scratch. But it will be good if you already know or have these things. The first thing is a GNU/Linux computer. I would recommend one to have Ubuntu https://ubuntu.com machine. This OS is becoming the OS of every good programmer. The second thing for you to do is to cultivate knowledge of GNU/Linux, you may read the tutorials on https://linuxjourney.com to get the knowledge. Once you have these knowledge, you will be in a better position to learn Ruby.

