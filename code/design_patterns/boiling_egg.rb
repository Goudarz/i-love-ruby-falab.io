require_relative "node.rb"

class BuyEggs < Node
  attr_accessor :time_in_minutes

  def initialize
    @time_in_minutes = 15
    super "Buy eggs"
  end
end

class BoilEggs < Node
  attr_accessor :time_in_minutes

  def initialize
    @time_in_minutes = 10
    super "Boil eggs"
  end
end

class PeelEggs < Node
  attr_accessor :time_in_minutes

  def initialize
    @time_in_minutes = 3
    super "Peel eggs"
  end
end

class ServeEggs < Node
  attr_accessor :time_in_minutes

  def initialize
    @time_in_minutes = 2
    super "Serve eggs"
  end
end

class BoiledEggs < Node
  def initialize
    super "Boiled eggs"
    add_child BuyEggs.new
    add_child BoilEggs.new
    add_child PeelEggs.new
    add_child ServeEggs.new
  end

  def total_time_in_minutes
    total = 0

    for child in self.children
      total += child.time_in_minutes
    end

    total
  end
end

boiled_eggs = BoiledEggs.new
puts "Time (in minutes) to make boiled eggs is #{boiled_eggs.total_time_in_minutes}."

