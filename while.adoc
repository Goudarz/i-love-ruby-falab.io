=== while

Whilefootnote:[kinda, while before your boss act like you work] loop is a loop that  does something till a condition is satisfied. Read the code below

[source, ruby]
----
include::code/while.rb[]
----

when executed, it produces the following output.

----
1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
----

Lets now see how an while loop works. A while loop normally has four important parts

1. Initialization
2. Condition check
3. Loop body
4. Updation

*Initialization*

See the statement `i=1` , here we initialize a variable named `i` and set it to value `1`.

*Condition check*

See the statement `while i<=10` , in this statement we specify that we are starting a while loop, this while loop on every iteration checks the value of `i` , if its less than or equal to `10` , the loops body gets blindly executed.

*Loop body*

Notice the `do` and `end` in the program, the `do` symbolizes the start of loop code block, the `end` symbolizes the end of loop code block. Between it we have some statements about which we will discuss soon. One of the statement is to print the value of `i`, which is accomplished by `print "#{i}, "`

*Updation*

Lets say that we forgot to include `i+=1`  in the loop body, at the end of each iteration the value of `i` will always remain 1 and i will always remain less than 10 hence the loop will be executed infinite number of times and will print infinite 1's, . In practical terms your program will crash with possible undesirable consequence. To avoid this we must include a updation statement. Here we have put `i+=1` which increments `i` by value one every time an iteration continues, this ensures that `i<=10` to become `false` at some stage and hence the loops stops executionfootnote:[Some cases a loop might be let to run infinite times (theoretically). Currently those things are outside the scope of this book.].

Hence we see that for a loop to work in an desirable manner we need to get these four parts into symphony.

