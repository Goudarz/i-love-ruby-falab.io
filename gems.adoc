== Gems

Gem is a package management stuff for ruby. For example you might want to do a stuff in ruby like say comparing two hashes, rather than writing a code by yourself you can search ruby gems repository located at http://rubygems.org

=== Searching a gem

So lets compare two hashes. There is a gem called hash_compare for comparing hashes. Now, you can goto http://rubygems.org and search for “hash compare” without the double quotes of course

image::gems-1e8f9.png[]

You will be getting a page as shown below, click on the hash compare link  and you will be directed to this page https://rubygems.org/gems/hash_compare

image::gems-7c3d0.png[]

So that's how you search for a gem. On the contrary, if you search for exact gem name hash_compare, http://rubygems.org has become smart now, and will take you straight to the gem page.

=== Installing gem

Now that you have found out the gem, how to install it? If you are in the gems page, https://rubygems.org/gems/hash_compare in this  case you will get the instruction to install it on your computer. You can install hash_compare gem by typing the following

----
$ gem install hash_compare
----

This will spit out the following stuff indicating that it has installed

----
Fetching: hash_compare-0.0.0.gem (100%)
Successfully installed hash_compare-0.0.0
1 gem installed
Installing ri documentation for hash_compare-0.0.0...
Installing RDoc documentation for hash_compare-0.0.0...
----

That is typing `gem install gem_name` in terminal should install most of the gems without a trouble.

=== Viewing Documentation

So you have installed a gem successfully, now you must know how to use it, where else is a great place to learn about a piece of ruby code than its documentation. If you are not sure about rdoc or ruby documentation, read the chapter <<Rdoc>>.
To see documentation for installed gem, you need to start a thing called gem server, which can be achieved by typing the following command in terminal

----
$ gem server
Server started at http://0.0.0.0:8808
----

The above command will spit a output saying that the server has been started. To know about hash compare gem goto this address http://0.0.0.0:8808 in your browser and search for hash_compare, else if you need to have a shorter way click this link http://0.0.0.0:8808/#hash_compare , when you click on hash_compare you will be directed here http://0.0.0.0:8808/doc_root/hash_compare-0.0.0/rdoc/index.html  , this is the documentation page for hash_compare gem.

There in that page, you will have sufficient (possibly) details about hash_compare gem.

=== Using gem

OK, to use the gemfootnote:[I will use it in irb, I don't have the patience to use it in a ruby file.] we in our terminal log in into irb using the following command

----
$ irb --simple-prompt
----

Next we will require hash compare command using the following command

[source, ruby]
----
>> require 'hash_compare'
=> true
----

And since the gem has been installed it says `true`. Now lets build two hashes a and be as shown below

[source, ruby]
----
>> a = { a: 1, b: 2}
=> {:a=>1, :b=>2}
>> b = { a: 1, b: 3, c: 2}
=> {:a=>1, :b=>3, :c=>2}
----

Now we add these to hash_compare object

[source, ruby]
----
>> h = HashCompare.new a, b
----

And find whats newly added using the newly_added function as shown below

[source, ruby]
----
>> h.newly_added
=> {:c=>2}
----

Well, that's it. You have learned how to use a gem.

=== The Gemfile

You must have heard of Ruby gems, you will be creating it and publishing it on http://rubygems.org  soon. Now lets see whats the use of Gemfile.

Checkout these files:

The first one is called requester.rb

[source, ruby]
----
# requester.rb

require 'rubygems'
require 'bundler/setup'
Bundler.require(:default)

resource = RestClient::Resource.new 'http://nothing.com'
p resource.get
----

The second one is the Gemfile which has the following content

[source, ruby]
----
source 'https://rubygems.org'

gem 'rest-client'
----

Put both in the same folder. If you look at requester.rb, it sends a request to  http://nothing.com  using the following lines
resource = RestClient::Resource.new 'http://nothing.com'

[source, ruby]
----
p resource.get
----

and prints it. For this to take place we need install a gem called rest-client using the command

[source, ruby]
----
$ gem install rest-client
----

and we need to require it requester.rb using the following line

[source, ruby]
----
require 'rest-client'
----

In other words the code must look as shown

[source, ruby]
----
require 'rest-client'

resource = RestClient::Resource.new 'http://nothing.com'

p resource.get
----

So why we have these three lines

[source, ruby]
----
require 'rubygems'
require 'bundler/setup'
Bundler.require(:default)
----

instead of one? Well let me explain. First this one is a simple project, which requires just one gem, in reality we might need tens of them in real life project. Before running the project if we do not have those gems in our system we need to manually check if each and every gem exists and install it. This might be simple for few gems, but the truth is if we have lots of gems we are going to hate it.

Welcome to the Ruby way, here is where the Gemfile comes as saviour. Lets analyze it. Open up the Gemfile, the first line is

[source, ruby]
----
source 'https://rubygems.org'
----

This tells the bundler (the thing that fetches and install gems) from where the gems must be fetched. Almost all ruby gems end up in http://rubygems.org. there are some bad guys however who like to have proprietary code and don't release it to public. Those suckers keep it for themselves, for them it will be something different.

Next we just list the gems needed in this format gem "<gem-name>" one by one. In this case we just have only one gem and so we list it as

[source, ruby]
----
gem 'rest-client'
----

Next in the ruby program that needs those gems we put this piece of code at the top

[source, ruby]
----
require 'rubygems'
require 'bundler/setup'
Bundler.require(:default)
----

I do not know what it does exactly, but this loads all gems specified in the Gemfile and thus making available readily all gems we need to run the program. Possibly if I learn more in the future I will update this section or most possibly not. All you have to do to fetch and install all the gems into the system is type this in the terminal :

----
$ bundle install
----

or in short

----
$ bundle
----

That's it. All the gems in its latest version will get installed in your system and will be available for the program that needs itfootnote:[To know more checkout this link http://bundler.io/gemfile.html]. Enjoy life!

==== Gemfile.lock

If you look at your link:code/fetch_data/Gemfile[Gemfile], all you have given is `gem 'rest-client'`, but take a look at link:code/fetch_data/Gemfile.lock[Gemfile.lock] shown below. It contains lot of stuff. What is this Gemfile.lock.

Well, its simple `rest-client` is not a stand alone Ruby package aka gem, it depends upon other gems. Gemfile.lock catalogs all the gems that were installed when we gave the `bundle` command. Lets analyze the Gemfile.lock that's shown below

[source, txt, linenums]
----
include::code/fetch_data/Gemfile.lock[]
----

So it starts with the keyword `GEM`, under which all the installed gems are listed. Check out line 2, it says `remote: https://rubygems.org/`, if you remember in the Gemfile you would have given `source 'https://rubygems.org'`, this is been recorded as remote repository in the lock file.

Checkout these lines (20 & 21)

----
PLATFORMS
  ruby
----

Here the platform is defined. What this book focuses upon is YARVfootnote:[https://en.wikipedia.org/wiki/YARV], its the default ruby interpreter, but its not the only one. There are other interpreters like JRuby that runs of Java. These information are logged under this `PLATFORM`.

The bundler's version is recorded too in these lines (26 & 27)

----
BUNDLED WITH
   1.16.2
----

You may check your bundler's version by typing the following commands in the terminal

----
$ bundle -v
Bundler version 1.16.2
----

Lets comeback to the `GEM` section. In the Gemfile we just needed rest-client and that's it. In the lock file on line 12 we see `rest-client (2.0.2)`, that is rest client of version 2.0.2 was installed, but in the following lines we see this too

----
rest-client (2.0.2)
  http-cookie (>= 1.0.2, < 2.0)
  mime-types (>= 1.16, < 4.0)
  netrc (~> 0.8)
----

Which means that `rest-client` depends on other gems namely `http-cookie`, `mime-types` and `netrc`. Lets take `http-cookie (>= 1.0.2, < 2.0)`, this means that `rest-client` of version `2.0.2` depends on `http-cookie` whose version must be greater than or equal to `1.0.2` and less than `2.0`. If you are wondering how gems are versioned and possibly want to version your software right, you may checkout Semantic Versioning here https://semver.org/.

Now lets see about `http-cookie`, look at lines 6 and 7, you see this

----
http-cookie (1.0.3)
  domain_name (~> 0.5)
----

Its means that `http-cookie` of version `1.0.3` has been installed and it depends on `domain_name` gem that's equal to 0.5 or greater. If you are confuse3d about `>`, `>=`, `~>`, this is what they mean

* = Equal To "=1.0"
* != Not Equal To "!=1.0"
* > Greater Than ">1.0"
* < Less Than "<1.0"
* >= Greater Than or Equal To ">=1.0"
* <= Less Than or Equal To "<=1.0"
* ~> Pessimistically Greater Than or Equal To "~>1.0"

So next you can trace what `domain_name (~> 0.5)` depends on and so on. The lock file builds a dependency tree and records the exact versions of gems that has been installed so that even if the software is bundled few years from now, it can install exact versions of gems from the lock file, thus guaranteeing it that it would work.

=== Creating a gem

Lets see how to create a very simple Gem. Lets create a Gem named hello_gem that just wishes Hello and welcome to the wonderful world of Ruby Gems. and does nothing more.

If you have downloaded this book, in folder named code/making_gem, you will see a folder named hello_gem. It has the following folder structure.

----
hello_gem/
  hello_gem.gemspec
  lib/
    hello_gem.rb
----

To practice, launch your terminal and navigate to the directory hello+gem/.If you look at the file link:code/making_gem/hello_gem/lib/hello_gem.rb[lib/hello_gem.rb], it has the following code

[source, ruby]
----
include::code/making_gem/hello_gem/lib/hello_gem.rb[]
----

If you look at the code, it just contains a line a that prints out `Hello and welcome to the wonderful world of Ruby Gems.`, that's it. Now lets come to the file that makes this program a gem. Look into the file link:code/making_gem/hello_gem/hello_gem.gemspec[hello_gem.gemspec]

[source, ruby]
----
include::code/making_gem/hello_gem/hello_gem.gemspec[]
----

Now lets examine it. In it we have described about the gem in ruby way. Some of the things we have defined are its name given by `s.name`; its homepage i.e mostly the place where its source code could be found or where the help and usage of this gem could be found, its given by `s.homepage`; the version number of the gem given by `s.version`; the date of release for this version given by `s.date`; a  brief summary of the gem given by `s.summary`; you can give a long description using `s.description` ; the names of authors can be given as a array as shown `s.authors = ["Karthikeyan A K"]`; email address for communication about the gem, given by `s.email`; and most important, all the program files this gem needs to run is given as a array like this `s.files = ["lib/hello_gem.rb"]`. In our case over here, we just need one file and its in lib directory.

We give `s.attrribute_name` because we have created a gem specification object using `Gem::Specification.new` and we have captured it it in variable `s` as shown

[source, ruby]
----
Gem::Specification.new do |s|
  # the specs goes here
end
----

Now all we need to do is to build the gemspec file to get our gem which we do it using the following command

----
$ gem build hello_gem.gemspec
----

You may see some warning messages as shown below, just ignore them, they are not so serious.

----
WARNING:  licenses is empty, but is recommended.  Use a license identifier from
http://spdx.org/licenses or 'Nonstandard' for a nonstandard license.
WARNING:  See http://guides.rubygems.org/specification-reference/ for help
  Successfully built RubyGem
  Name: hello_gem
  Version: 0.0.0
  File: hello_gem-0.0.0.gem
----

If you notice, in the same folder you will see a file named _hello_gem-0.0.0.gem_, the _hello_gem_ part of the file name comes from `s.name` specified in the gemfile, and the _0.0.0_ comes from `s.version` specified in the gemfile.

Now lets install gem using the command below

----
$ gem install hello_gem-0.0.0.gem
----

When installing you will get the output shown below

----
Successfully installed hello_gem-0.0.0
Parsing documentation for hello_gem-0.0.0
Installing ri documentation for hello_gem-0.0.0
Done installing documentation for hello_gem after 0 seconds
1 gem installed
----

Let's launch irb to test the gem

----
$ irb --simple-prompt
----

Now in irb lets require the gem as shown below

----
>> require "hello_gem"
Hello and welcome to the wonderful world of Ruby Gems.
=> true
----

Now you can see a a output _Hello and welcome to the wonderful world of Ruby Gems._. Congrajulations, we have built our own gem. We can now distribute it to the entire world.

=== Publishing your gem

So we have created our gem. Now lets see how to publish one. As a first step you must goto https://rubygems.org/sign_up and  create an account. Remember your user name and password. To make your gem unique lets name the gem _<user name>_hello_. My username is mindaslab, and hence my gem name is mindaslab_hello.

This gem is very similar to the previous _hello_gem_ gem. It has the following folder structure. Naviate to _mindaslab_hello/_ folder

----
mindaslab_hello/
  mindaslab_hello.gemspec
  lib/
    mindaslab_hello.rb
----

You may like to go through link:code/making_gem/mindaslab_hello/mindaslab_hello.gemspec[mindaslab_hello.gemspec] and
link:code/making_gem/mindaslab_hello/mindaslab_hello.gemspec[mindaslab_hello.rb]. Its better you modify the gemspec file so that your name and email appears rather than mine.

Now to build the gem type in the following command

----
$ gem build mindaslab_hello.gemspec
----

You should be seeing a file named _mindaslab_hello-0.0.0.gem_ generated in the same folder. The `build` command will throw an output shown below in the terminal.

----
WARNING:  licenses is empty, but is recommended.  Use a license identifier from
http://spdx.org/licenses or 'Nonstandard' for a nonstandard license.
WARNING:  no homepage specified
WARNING:  See http://guides.rubygems.org/specification-reference/ for help
  Successfully built RubyGem
  Name: mindaslab_hello
  Version: 0.0.0
  File: mindaslab_hello-0.0.0.gem
----

Now lets push the gem to ruby gems website. All you need to do is give the command `gem push <generated gem name>` in the terminal as shown.

----
$ gem push mindaslab_hello-0.0.0.gem
----

You will be prompted for Rubygems user name and password, provide them and you will be seeing a output as shown below.

----
Pushing gem to https://rubygems.org...
Successfully registered gem: mindaslab_hello (0.0.0)
----

You can go to https://rubygems.org and type in your gem name to search

image::gems-817e2.png[]

You will be taken to your gem page as shown

image::gems-07593.png[]

Now since the gem is globally available on the internet, you can install your gem with `gem install <gemfile name>` as shown below

----
$ gem install mindaslab_hello
----

It should throw output something as shown below

----
Successfully installed mindaslab_hello-0.0.0
Parsing documentation for mindaslab_hello-0.0.0
Installing ri documentation for mindaslab_hello-0.0.0
Done installing documentation for mindaslab_hello after 0 seconds
1 gem installed
----

To test your gem launch your irb.

----
$ irb --simple-prompt
----

Then require your gem

----
>> require "mindaslab_hello"
----

If you see an output as shown below, drop me an email :) We did it. Hi five!!

----
Hello from Karthikeyan A K.
I am from Chennai, India.
=> true
>>
----

=== More complex gems

In reality you will be needing more than 1 ruby file to package in your gem. One way to package is to list all the files in gemspec file, or is there a better way? To find out lets write a gem called shapes.

We will be writing three classes named `Circle`, `Rectangle`, `Square` in files called link:code/making_gem/shapes/lib/models/circle.rb[circle.rb], link:code/making_gem/shapes/lib/models/rectangle.rb[rectangle.rb] and link:code/making_gem/shapes/lib/models/square.rb[square.rb], these files we put it in a folder called `models/` and require them in a file called link:code/making_gem/shapes/lib/shapes.rb[shapes.rb], you can see the folder structure as shown below.

----
shapes/
  shapes.gemspec
  lib/
    shapes.rb
    models/
      circle.rb
      rectangle.rb
      square.rb
----

Now all ruby files in the `lib/` folder and the files in `lib/models/` folder must be included in the gemspec file. Look at the gemspec file below

[source, ruby]
----
include::code/making_gem/shapes/shapes.gemspec[]
----

Look at the line `s.files = Dir["*/*.rb", "*/*/*.rb"]`, here we do not write very verbose list of files. Instead we use a the `Dir`footnote:[https://ruby-doc.org/core-2.5.3/Dir.html] library in Ruby to do the task.

To see how the `Dir` works, launch your irb in your `shapes/` directory and type the following

----
>> Dir["*/*.rb", "*/*/*.rb"]
----

You would see that it neatly gives out the list of ruby files in primary and secondary sub folders.

----
=> ["lib/shapes.rb", "lib/models/square.rb", "lib/models/rectangle.rb", "lib/models/circle.rb"]
----

Thus we can use such tricks to include lot of files in our gem. Lets now build our `gemspec` file using the following command

----
$ gem build shapes.gemspec
----

As shown below we get a good build

----
WARNING:  licenses is empty, but is recommended.  Use a license identifier from
http://spdx.org/licenses or 'Nonstandard' for a nonstandard license.
WARNING:  no homepage specified
WARNING:  See http://guides.rubygems.org/specification-reference/ for help
  Successfully built RubyGem
  Name: shapes
  Version: 0.0.0
  File: shapes-0.0.0.gem
----

Lets install our gemfile ash shown  below

----
$ gem install shapes-0.0.0.gem
----

As you see it get successfully installed

----
Successfully installed shapes-0.0.0
Parsing documentation for shapes-0.0.0
Installing ri documentation for shapes-0.0.0
Done installing documentation for shapes after 0 seconds
1 gem installed
----

Lets test out gem by writing a program called link:code/making_gem/testing_shapes.rb[testing_shapes.rb] as shown below

[source, ruby]
----
include::code/making_gem/testing_shapes.rb[]
----

Now lets run it

----
$ ruby testing_shapes.rb
----

We get output as shown below.

----
Area of square = 49
Area of circle = 153.93804002589985
----

So we have seen how to build a bit more complex gem with more files in it.

=== Requiring other gems



=== Uninstalling a Gem

Finally to uninstall a gem just type `gem uninstall <gemname>`, so by typing

----
$ gem uninstall shapes
----

You will get a output as shown

----
Successfully uninstalled shapes-0.0.0
----

Which indicates the gem has been uninstalled successfully.

