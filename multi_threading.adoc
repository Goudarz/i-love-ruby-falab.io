== Multi Threading

Usually a program is read line by line and is executed step by step by the computer. At any given point of time the computer executes only one instruction1. When technology became advanced it became possible to execute many instructions at once, this process of doing many things at the same time is called multi processing or parallel processing.
Imagine that you are tasked with eating 5 pizzas. It would take a long time for you to do it. If you could bring your friends too, then you people can share the load. If you can form a group of 20 people, eating 5 pizzas becomes as easy as having a simple snack. The time required to complete the assigned task gets reduced drastically.

In your Ruby programming you can make the interpreter execute code in a parallel fashion. The process of executing code parallel is called multi threading. To show how multithreading works type the program below in text editor and execute it.

[source, ruby]
----
include::code/multithreading.rb[]
----

Here is the programs output

----
This code comes after thread
1
2
3
4
5
6
7
8
9
10
----

Unlike other programs this program will take 10 seconds to execute. Take a look at the program and output. The `puts "This code comes after thread"` comes after

[source, ruby]
----
a = Thread.new{
  i = 1;
  while i<=10
    sleep(1)
    puts i
    i += 1
  end
}
----

Yet it gets printed first. In the statements shown above we create a new thread named a in which we use a `while` loop to print from 1 to 10. Notice that we call a function called `sleep(1)` which makes the process sleep or remain idle for one second. A thread was created, while the thread is running, the Ruby interpreter checks the main thread and its comes across `puts "This code comes after thread"` and hence it prints out the string and in parallel as it executes the new thread a created by us, so 1 to 10 gets printed as we have inserted `sleep(1)` each loop takes about 1 second to execute. So after 10 seconds the thread a is finished.

The `a.join` tells the Ruby interpreter to wait till the thread finishes execution. Once the execution of thread a is over the statements after `a.join` (if any) gets executed. Since there is no statement after it the program terminates.

Here is another program that clearly explains about multithreading.  Go through the program carefully, try to understand how it works, I will explain it in a moment

[source, ruby]
----
include::code/multithreading_1.rb[]
----

Look at the highlighted code, we have created two threads `t1` and `t2`. Inside thread `t1` we call  the method `func1()` and in thread `t2` we call the method `func2()`, by doing so we are executing both `func1()` and `func2()` in parallel. When executed this is how the output will look likefootnote:[You may get a different output as you may be executing the program at a different time]

----
Started At Sun Apr 25 09:37:51 +0530 2010
func1 at: Sun Apr 25 09:37:51 +0530 2010
func2 at: Sun Apr 25 09:37:51 +0530 2010
func2 at: Sun Apr 25 09:37:52 +0530 2010
func1 at: Sun Apr 25 09:37:53 +0530 2010
func2 at: Sun Apr 25 09:37:53 +0530 2010
func1 at: Sun Apr 25 09:37:55 +0530 2010
End at Sun Apr 25 09:37:57 +0530 2010
----

As you can see from the output, the outputs printed by `func1` and `func2` are interlaced which proves that they have been executed in parallel. Note that in `func1` we have made the thread sleep for 2 seconds by giving `sleep(2)` and in `func2` we have made the thread sleep for 1 second by giving `sleep(1)`.

I have made small changes in link:code/multithreading_1.rb[multithreading_1.rb] to produce link:code/multithreading_2.rb[multithreading_2.rb] which gives almost the same result of link:code/multithreading_1.rb[multithreading_1.rb], so here is its code:

[source, ruby]
----
include::code/multithreading_2.rb[]
----

Instead of using two functions `func1` and `func2`, I have written a single function called `func` which accepts a name and time delay as input. A loop in it prints out the name passed and the time instant at which the statement gets executed. Notice the these statements

[source, ruby]
----
t1 = Thread.new{func "Thread 1:", 2}
t2 = Thread.new{func "Thread 2:", 3}
----

we create two threads `t1` and `t2`. In thread `t1` we call the function `func` and pass along the name `Thread 1:` and tell it to sleep for 2 seconds. In thread `t2` we call the same `func` and pass name as  `Thread 2:` and tell it to sleep for 3 seconds in each loop iteration. And when executed the program produces the following output

----
Started At Sun Apr 25 09:44:36 +0530 2010
Thread 1: Sun Apr 25 09:44:36 +0530 2010
Thread 2: Sun Apr 25 09:44:36 +0530 2010
Thread 1: Sun Apr 25 09:44:38 +0530 2010
Thread 2: Sun Apr 25 09:44:39 +0530 2010
Thread 1: Sun Apr 25 09:44:40 +0530 2010
Thread 2: Sun Apr 25 09:44:42 +0530 2010
End at Sun Apr 25 09:44:45 +0530 2010
----

Which is very similar to output produced by link:code/multithreading_1.rb[multithreading_1.rb].

=== Scope of thread variables

A thread can access variables that are in the main process take the following program (link:code/thread_variables.rb[thread_variables.rb]) as example

[source, ruby]
----
include::code/thread_variables.rb[]
----

Output

----
Before thread variable = 0
After thread variable = 5
----

Type the program and run it. It will produce the result shown above. As you can see from the program we initialize a variable named `variable` to 0 before we create the thread. Inside the thread we change the value of the `variable` to 5. After the thread block we print variable's value which is now 5. This program shows us that you can access and manipulate a variable that's been declared in the main thread.

Lets now see if a variable created inside a thread can be accessed outside the scope of it? Type in the following program (link:code/thread_variables_1.rb[thread_variables_1.rb]) and execute it

[source, ruby]
----
include::code/thread_variables_1.rb[]
----

Output

----
Before thread variable = 0
Inside thread thread_variable = 72
=================
After thread
variable = 5
thread_variables_1.rb:13: undefined local variable or method `thread_variable' for main:Object (NameError)
----

In the program above we see that we have created a variable named thread_variable in the thread `a`, now we try to access it in the following line:

[source, ruby]
----
puts "thread_variable = #{thread_variable}"
----

As you can see the output that the program / Ruby interpreter spits an error as shown:

----
thread_variables_1.rb:13: undefined local variable or method `thread_variable' for main:Object (NameError)
----

It says there is an undefined local variable or method named `thread_variable`. This means that the statement in main thread is unable to access  variable declared in the thread `a`. So from the previous two examples its clear that a thread can access a variable declared in the main thread whereas a variable declared in the thread's scope cannot be accessed by statement in main scope.

=== Thread Exclusion

Lets say that there are two threads that share a same resource, let the resource be a variable. Lets say that the first thread modifies the variable, while its modifying the second thread tries to access the variable, what will happen? The answer is simple and straight forward, though the program appears to run without errors you may not get the desired result. This concept is difficult to grasp, let me try to explain it with an example. Type and execute link:code/thread_exclusion.rb[thread_exclusion.rb]

[source, ruby]
----
include::code/thread_exclusion.rb[]
----

Output

----
difference = 127524
----

Read the program carefully. Before we start any thread, we have three variables `x`, `y` and diff that are assigned to value 0. Then in the first thread we start a loop in which we increment the value of `x` and `y`. In another thread we find the difference between `x` and `y` and save it in a variable called `diff`. In the first thread `x` and `y` are incremented simultaneously, hence the statement `diff += (x-y).abs` should add nothing to variable `diff` as `x` and `y` are always equal and their difference should always be zero, hence the absolute value of their difference will also be zero all the time.

In this program we don't wait for the threads to join (as they contain infinite loop), we make the main loop sleep for one second using the command `sleep(1)` and then we print the value of diff in the following statement

[source, ruby]
----
puts "difference = #{diff}"
----

One would expect the value to be zero but we got it as 127524, in your computer the value could be different as it depends on machine speed, what processor its running and other parameters. But the moral is `diff` that should be zero has some value, how come?

We see in the first loop that `x` is incremented and then `y` is incremented, lets say that at an instant `x` value is 5 and y value is 4, that is `x` had just got incremented in the statement `x += 1` and now the Ruby interpreter is about to read and execute `y += 1` which will make `y` from 4 to 5. At this stage the second thread is executed by the computer. So in the statement

[source, ruby]
----
diff += (x-y).abs
----

putting `x` as 5 and `y` as 4 will mean that `diff` will get incremented by 1. In similar fashion while the main loop sleeps for one second, the two thread we have created would have finished thousands of loop cycles, hence the value of `diff` would increased significantly. That's why we get the value of diff as a large number.

Well, we have seen how not to write the program in a wrong way, lets now see how to write it in the right way. Our task now is to synchronize the two threads that we have created so that one thread does not access other threads resources when the other is in middle of some busy process. To do so we will be using a thing called Mutex which means Mutual Exclusion. Type the following program link:code/thread_exclusion_1.rb[thread_exclusion_1.rb] in text editor and execute it

[source, ruby]
----
include::code/thread_exclusion_1.rb[]
----

Output

----
difference = 0
----

As you see above, we get output as `difference = 0`, which means that `diff` variable is zero. Some thing prevented the second thread from accessing `x` and `y` in the first thread while the first one was busy. Some how the two threads have learned to share their resources in a proper way. Study the code carefully, we see that we have included a thread package by typing

[source, ruby]
----
require 'thread'
----

Next we have created a Mutex variable named mutex using the following command

[source, ruby]
----
mutex = Mutex.new
----

Well then the code is as usual except inside the threads. Lets look into the first thread

[source, ruby]
----
Thread.new {
  loop do
    mutex.synchronize do
      x += 1
      y += 1
    end
  end
}
----

We see that the statements `x += 1` and `y += 1` are enclosed in `mutex.synchronize` block. In similar way, the code computing the difference of `x` and `y` is also enclosed in `mutex.synchronize` block as shown:

[source, ruby]
----
Thread.new {
  loop do
    mutex.synchronize do
      diff += (x-y).abs
    end
  end
}
----

By doing so that we tell the computer that there is a common resource is shared by these two threads and one thread can access the resource only if other releases it. By doing so, when ever the difference (`diff`) is calculated in `diff += (x-y).abs`, the value of `x` and `y` will always be equal, hence diff never gets incremented and stays at zero forever.

=== Deadlocks

Have you ever stood in a queue,or waited for something. One place we all wait is in airport for our luggage's to be scanned and cleared. Lets say that the luggage scanning machine gets out of order and you are stuck in airport. You are expected to attend an important company meeting and you have the key presentation and you must give an important talk. Since your baggage scanner failed the resource needed by you is not available to you, and you have the key knowledge to talk in the meeting and hence the meeting gets screwed up. One among the meeting might have promised to take his family to a movie, he might return late after the delayed meeting and hence all screw up.

Imagine that the baggage scanner, you, a person who must listen to your meeting are threads of a Ruby program. Since the baggage scanner wont release a resource (your bag) your process gets delayed and since you are delayed many other processes that depends on you are delayed. This situation is called dead lock. It happens in Ruby programs too.
When ever situation like this arises people wait rather than to rush forward. You wait for your bag to be released, your company waits for your arrival and the mans family waits for him to take them to movie. Instead of waiting if people rush up it will result in chaos.
Ruby has a mechanism to avoid this chaos and to handle this deadlock. We use a thing called condition variable. Take a look at the program (link:code/thread_condition_variable.rb[thread_condition_variable.rb]) below, type it and execute it.

[source, ruby]
----
include::code/thread_condition_variable.rb[]
----

Output

----
Thread a now waits for signal from thread b
(Now in thread b....)
Thread b is using a resource needed by a, once its done it will signal to a
b Signaled to a to acquire resource
a now has the power to use the resource
----

Study the program and output carefully. Look at the output. First the statement in Thread `a` gets executed and it prints that Thread `a` is waiting for Thread `b` to signal it to continue. See in thread `a` we have written the code `c.wait(mutex)` , where `c` is the Condition Variable declared in the code as follows:

[source, ruby]
----
c = ConditionVariable.new
----

So now thread a waits, now the execution focused on thread b when the following line is encountered in Thread `b

[source, ruby]
----
puts "Thread b is using a resource needed by a, once its done it will signal to a"
----

it prints out that thread `b` is using some resource needed by `a`, next thread `b` sleeps for 4 seconds because we have given `sleep(4)` in it. This `sleep` statement can be avoided, I have given a `sleep` because while the reader executes the program it make him wait and gives a feel that how really condition variable works.

Then after 4 seconds, Thread `b` signals to Thread `a`, its wait can end using the statement `c.signal`. Now Thread a receives the signal and can execute its rest of its statements, i.e after `c.signal`, Thread `a` and Thread `b`  can execute simultaneously.

=== Creating multiple threads

Lets say that you have a situation where you have to create many threads, and it must be done in elegant way, say that a situation might arise you don't even know how many threads could be created, but you must create them and the program should wait for them to join and then exit, so lets see how to code that.

So type the program below into your text editor and run it

[source, ruby]
----
include::code/many_threads.rb[]
----

Output

----
Hi
Hello
Hello
Hello
Hi
Hi
----

If you are not getting the exact output as above, do not worry as there is some randomness in the program. Let me explain the program so that it becomes clear to you.

First we declare an array that will hold the threads as shown below

[source, ruby]
----
threads = []
----

Next we will put an value into threads array using the function `launch_thread` as shown below

[source, ruby]
----
threads << launch_thread("Hi")
----

Lets analyze what goes on in the `launch_thread` function, first we have a function as shown below

[source, ruby]
----
def launch_thread string
  …....
end
----

We are returning a variable called `thread` from the function

[source, ruby]
----
def launch_thread string
  return thread
end
----

We assign a newly created thread to the variable thread

[source, ruby]
----
def launch_thread string
  thread = Thread.new do
  end
  return thread
end
----

We put some code inside the newly created thread

[source, ruby]
----
def launch_thread string
  thread = Thread.new do
    3.times do
      puts string
      sleep rand(3)
    end
  end
  return thread
end
----

That's it. So in short we create the thread, run the code in it and return it which gets stored in `threads` array. The same stuff happens in this line too

[source, ruby]
----
threads << launch_thread("Hello")
----

Now we have to wait for each thread to join the main program (or the main thread). So we write the joining code as shown

[source, ruby]
----
threads.each {|t| t.join}
----

This will wait till all threads have completed and joins with the main code.

So we have written a program that can create as many threads as we want (in the above case two), and all the threads will be gathered into an array and the main program will wait till all threads have completed and joined with it and then would exit.

Now take look at link:code/many_threads_1.rb[many_threads_1.rb] and link:code/many_threads_2.rb[many_threads_2.rb], execute them and explain to yourself how they work. Better write a good explanation for them and mail to me so that I can put it in this book.

[source, ruby]
----
include::code/many_threads_1.rb[]
----

[source, ruby]
----
include::code/many_threads_2.rb[]
----

=== Thread Exception

When ever there is an exception when the program is running, and if the exception isn't handled properly the program terminates. Lets see what happens when there is a exception in a  thread. Type the code link:code/thread_exception_true.rb[thread_exception_true.rb] in text editor and execute it.

[source, ruby]
----
include::code/thread_exception_true.rb[]
----

Output

----
5
6
8
12
25
thread_exception_true.rb:8:in `/': divided by 0 (ZeroDivisionError)
	from thread_exception_true.rb:8
	from thread_exception_true.rb:4:in `initialize'
	from thread_exception_true.rb:4:in `new'
	from thread_exception_true.rb:4
----

Notice in the program we create a thread named `t`, and if you are quiet alert we haven't got `t.join` in the program. Instead of waiting for the thread to join we wait long enough for the thread to complete. For the thread to complete we wait for 10 seconds by using the statement `sleep(10)`.

Notice the line `t.abort_on_exception = true` where we set that if there raises an exception in the thread `t`, the program must abort. Lets now analyze whats in thread `t`. Thread `t` contains the following code

[source, ruby]
----
t = Thread.new do
  i = 5
  while i >= -1
    sleep(1)
    puts 25 / i
    i -= 1
  end
end
----

Notice that we divide 25 by `i` and put out the result of the division. `i` is decremented by 1 in each loop iteration, so when `i` becomes zero and when 25 is divided by it, it will raise an exception. So at the sixth iteration of the loop, 25 is divided by zero, an exception is raised and the program stops by spiting out the following

----
thread_exception_true.rb:8:in `/': divided by 0 (ZeroDivisionError)
	from thread_exception_true.rb:8
	from thread_exception_true.rb:4:in `initialize'
	from thread_exception_true.rb:4:in `new'
	from thread_exception_true.rb:4
----

This happens because we have set `t.abort_on_exception` to `true` (see the highlighted code). What happens if we set it as `false`. Take a look at the program link:code/thread_exception_false.rb[thread_exception_false.rb]. In the program we have set `t.abort_on_exception` as `false`. Type the program in text editor and run it

[source, ruby]
----
include::code/thread_exception_false.rb[]
----

Take a look at output

----
5
6
8
12
25
Program completed
----

As you can see from the output there is no trace of exception occurrence4. The code that comes after thread `t` gets executed and we get output that says `Program Completed`. This is because we have set `abort_on_exception` to be `false`.

You can see from the last two programs that we haven't used `t.join` , instead we have waited long enough for the thread to terminate. This is so because once we join thread (that causes) exception with the parent (in this case the main) thread, the exception that arises in the child thread gets propagated to the parent / waiting thread so `abort_on_exception` has no effect even it set to false. So when ever exception raises it gets reflected on our terminal.

=== Thread Class Methods

There are certain thread methods which you can use to manipulate the properties of the thread. Those are listed below. If you can't understand a bit of it, never worry.

:===
Sno. : Method : What it does


1.
Thread.abort_on_exception
Returns the status of the global abort on exception condition. The default is false. When set to true, will cause all threads to abort (the process will exit(0)) if an exception is raised in any thread.

2.
Thread.abort_on_exception=
When set to true, all threads will abort if an exception is raised. Returns the new state.

3.
Thread.critical
Returns the status of the global thread critical condition.

4.
Thread.critical=
Sets the status of the global thread critical condition and returns it. When set to true, prohibits scheduling of any existing thread. Does not block new threads from being created and run. Certain thread operations (such as stopping or killing a thread, sleeping in the current thread, and raising an exception) may cause a thread to be scheduled even when in a critical section.

5.
Thread.current
Returns the currently executing thread.

6.
Thread.exit
Terminates the currently running thread and schedules another thread to be run. If this thread is already marked to be killed, exit returns the Thread. If this is the main thread, or the last thread, exit the process.

7.
Thread.fork { block }
Synonym for Thread.new

8.
Thread.kill( aThread )
Causes the given aThread to exit

9.
Thread.list
Returns an array of Thread objects for all threads that are either runnable or stopped. Thread.

10.
Thread.main
Returns the main thread for the process.

11.
Thread.new( [ arg ]* ) {| args | block }
Creates a new thread to execute the instructions given in block, and begins running it. Any arguments passed to Thread.new are passed into the block.

12.
Thread.pass
Invokes the thread scheduler to pass execution to another thread.

13.
Thread.start( [ args ]* ) {| args | block }
Basically the same as Thread.new . However, if class Thread is subclassed, then calling start in that subclass will not invoke the subclass's initialize method.

14.
Thread.stop
Stops execution of the current thread, putting it into a sleep state, and schedules execution of another thread. Resets the critical condition to false
:===

=== Thread Instance Methods

Since everything in Ruby is an object, ia thread too is an object. Like many objects, threads have functions or methods which can be called to access or set a property in thread. Some of the functions and their uses are listed below (thr is an instance variable of the Thread class):

|===
| Sno. | Method | What it does

| 1
| thr.alive?
| This method returns true if the thread is alive or sleeping. If the thread has been terminated it returns false.

| 2
| thr.exit
| Kills or exits the thread

| 3
| thr.join
| This process waits for the thread to join with the process or thread that created the child thread. Once the child thread has finished execution, the main thread executes the statement after thr.join

| 4
| thr.kill
| Same ad thr.exit

| 5
| thr.priority
| Gets the priority of the thread.

| 6
| thr.priority=
| Sets the priority of the thread. Higher the priority, higher preference will be given to the thread having higher number.

| 7
| thr.raise( anException )
| Raises an exception from thr. The caller does not have to be thr.

| 8
| thr.run
| Wakes up thr, making it eligible for scheduling. If not in a critical section, then invokes the scheduler.

| 10
| thr.wakeup
| Marks thr as eligible for scheduling, it may still remain blocked on I/O, however.

| 11
| thr.status
| Returns the status of thr: sleep if thr is sleeping or waiting on I/O, run if thr is executing, false if thr terminated normally, and nil if thr terminated with an exception.

| 12
| thr.stop?
| Waits for thr to complete via Thread.join and returns its value.

| 13
| thr[ aSymbol ]
| Attribute Reference - Returns the value of a thread-local variable, using either a symbol or a aSymbol name. If the specified variable does not exist, returns nil.

| 14
| thr[ aSymbol ] =
| Attribute Assignment - Sets or creates the value of a thread-local variable, using either a symbol or a string.

| 15
| thr.abort_on_exception
| Returns the status of the abort on exception condition for thr. The default is false.

| 16
| thr.abort_on_exception=
| When set to true, causes all threads (including the main program) to abort if an exception is raised in thr. The process will effectively exit(0).

|===

